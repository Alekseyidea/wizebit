
    let FAQ_seen = false;


    let ResizeFn = fn => {
        fn();
        $(window).resize(function(){
            fn();
        });
    }
   

    let community_btn = $('.community-section__btn');


    let DEVICE = 'desktop';
    function DetectDevice(){
        if( $('.small-desktop-detect-js').is(':visible'))
            DEVICE = 'small';
        if( $('.old-desktop-detect-js').is(':visible'))
            DEVICE = 'old';
        if($('.mobile-detect-js').is(':visible'))
            DEVICE = 'mobile';
    }
    ResizeFn(DetectDevice);
    

    $('#fullpage').fullpage({
        anchors: ['firstSection', 'vision', 'features','wizebox-mini', 'save', 'spec','community','token','development','team','faq','footer'],
        menu: '#menu',
        css3: true,
        scrollingSpeed: 1000,
        onLeave: function (index,nextIndex, direction) {

            let title;
            let content;


            if( index === 2 && nextIndex === 1 ){
                $('.page__header').addClass('firstSlide');
            }else {
                $('.page__header').removeClass('firstSlide');
            }

            if( index === 1 && nextIndex === 2 ){
                title = '.section .about-section__title--';
                content = '.section .about-section__content--';

                $(title+'first').addClass('animated fadeInDownBig');

                $(content+'left').addClass('animated fadeInLeft');
                $(content+'right').addClass('animated fadeInRight');

                $(title+'second').addClass('animated fadeInUpBig');
                $(content+'second').addClass('animated fadeInUpBig');

                $(content+'third').addClass('animated fadeInUpBig');


                $(title+'first').css('animation-delay', '.2s');
                $(content+'left').css('animation-delay', '.4s');
                $(content+'right').css('animation-delay', '.4s');

                $(title+'second').css('animation-delay', '.8s');
                $(content+'second').css('animation-delay', '1s');

                $(content+'third').css('animation-delay', '1.2s');

            }
            if( index === 3 && nextIndex === 4 ){
                $("#security-section-video").YTPPlay();
            }




         },

        afterLoad: function(anchorLink, index){

           // console.log(anchorLink);

            if (anchorLink === 'firstSection'){
                $("#bgndVideo").YTPPlay();
            }

            if(anchorLink === 'features' && !$('.page__description').hasClass('animation-seen')){

                setTimeout(function () {
                    $('#wizeBox').fadeIn();
                    new Vivus(
                        'wizeBox',
                        {
                            start: 'autostart',
                            forceRender: false,
                            dashGap: 0,
                            //type: 'scenario-sync',
                            duration: 150
                        }, function () {
                            if (window.console) {
                                $('.page__description').addClass('animation-seen');

                                let image = $('.description-section__img img');

                                let svg = $('.description-section__img svg');

                                let sectionTitle = $('.description-section__title-wrapper');





                                let page = '#join-line-spec ';
                                let page2 = '#join-line-spec ';
                                let section = $('.description-section');
                                let joinLine_1 = $('.join-line--description.join-line');




                                function fnWidth() {

                                    let joinLineWidth = $(window).width() - section.offset().left - section.width() - 50;
                                    let joinLineHeight = section.offset().top + 49;
                                   // console.log(joinLineWidth);
                                    joinLine_1.height(joinLineHeight-15 + 'px').width(joinLineWidth + 'px');
                                }

                                fnWidth();



                                let ImageShow = function () {
                                    TweenMax.to(image, 1, {opacity:1});
                                    TweenMax.to(svg, 1, {opacity:0});
                                };

                                let showTitle = function () {
                                    TweenMax.to(sectionTitle, 1, {opacity:1})
                                };

                                let ImageLeft = function () {


                                    $('.description-section__item').addClass(' animated fadeInLeft');

                                    let startAnimationLeft = function () {
                                        TweenMax.staggerTo(".description-section__item",.5, {onComplete: completeLeftAnimation }, .7, allComplete);

                                        function completeLeftAnimation() {
                                            //this.target.className += ' animated fadeInLeft';
                                            let line = this.target.getElementsByClassName('description-section__line')[0];

                                            //this.target.style.animationDuration = '.5s';

                                            let lineAnimation = function () {
                                                if(line){
                                                    TweenMax.to(line, 0, {opacity:'1'});
                                                    TweenMax.to(line, 1, {width:'200%',opacity:'1'});
                                                }else {
                                                    return false;
                                                }

                                            };
                                            TweenLite.delayedCall(0, lineAnimation);

                                        }
                                        function allComplete() {
                                            let fnLineHorizontal = function () {

                                                let page = '.page__description ';

                                                    let horizontalLine = $(page+'.join-line__gorizontal');


                                                    let fnPulse = function () {
                                                        let pulse = $(page+'.pulse');

                                                        TweenMax.to(pulse, 1, {opacity:'1'});
                                                    };
                                                    let fnVerticalLine = function () {
                                                        let  verticalLine = $(page+'.join-line__vertical');
                                                        TweenMax.to(verticalLine, 1, {height:'100%',onComplete: fnPulse});
                                                    };
                                                    let fnSquare = function () {
                                                        let square = $(page+'.join-line__square');

                                                        TweenMax.to(square, 1, {opacity:'1',onComplete: fnVerticalLine});
                                                    };

                                                    TweenMax.to(horizontalLine, 1, {width:'100%',onComplete: fnSquare});
                                            };
                                           // console.log('all complete');

                                            TweenLite.delayedCall(0,fnLineHorizontal);
                                        }

                                    };

                                    TweenMax.to(image, .5, {left:0, onComplete:startAnimationLeft});
                                };





                                ImageShow();
                                TweenLite.delayedCall(2, showTitle );
                                TweenLite.delayedCall(3, ImageLeft);

                               // console.log('Animation finished. [log triggered from callback]');
                            }
                        });
                },100);
            }


            if (anchorLink === 'wizebox-mini'){


                function AnimationLines(id,height=100,width=100,callback=false) {
                    const block = '.wizebox-mini__line--';
                    const title =  $(`${id} .wizebox-mini__title`);
                    const hLine =  $(`${id} ${block}horizontal`);
                    const vLine =  $(`${id} ${block}vertical`);
                    const circle =  $(`${id} .wizebox-mini__circle`);

                    const line = {
                      'height': height + '%',
                      'width': width + '%',
                    };


                    let fnLine = ()=> {

                        const fnT = ()=> {
                            TweenMax.to(title,1,{opacity:1});
                        };

                        const fnW= ()=> {
                            TweenMax.to(hLine,1,{width:line.width, onComplete: fnT});
                        };

                        let fnH = function(){
                            TweenMax.to(vLine,1,{height:line.height, onComplete: fnW});
                        } ;


                        TweenMax.to(circle,1,{opacity:1, onComplete: fnH});

                    };

                    TweenLite.delayedCall(0,fnLine);

                }

                AnimationLines('#rotor');
                AnimationLines('#button');
                AnimationLines('#ports');
                AnimationLines('#speakers',81);
            }

            if (anchorLink === 'save'){


                //youtube video width

                function fnYoutubeWidth(){
                    let youtubeWidth = $('.security-section__inner').offset().left;
                    $('.security-section__youtubeVideo').width(youtubeWidth);
                }
   
                ResizeFn(fnYoutubeWidth);


                let page = '#join-line-security ';
                let page2 = '#join-line-security-2 ';
                let section = $('.security-section');
                let joinLine_1 = $('#join-line-security.join-line--save');
                let joinLine_2 = $('#join-line-security-2.join-line--save');


                function fnWidth() {
                    let joinLineWidth = $(window).width() - section.offset().left - section.width() - 25;
                    let joinLineWidth2 = $(window).width() - section.offset().left - section.width() - 50;
                    let joinLineHeight = section.offset().top + 49;
                    joinLine_1.height(joinLineHeight + 'px').width(joinLineWidth + 'px');

                    let num;
                    let num2;

                    function fnNum(){
                        switch (DEVICE) {
                            case 'small':
                                num = 80;
                                num2 = 85;
                                break;
                            case 'old':
                                num = 80;
                                num2 = 85;
                                break;
                            default:
                                num = 300;
                                num2 = 47;
                        }
                                       
                    }
                    fnNum();

                    $(window).resize(function(){
                        fnNum();
                    });

                    joinLine_2.height(joinLineHeight+num + 'px').width(joinLineWidth2 + 'px').css({
                        'margin-top': -num-num2 + 'px'
                    });
                }

                fnWidth();


                let fnLine = function () {

                    const lineSpeed = 1;
                    const squareSpeed = .5;



                    let horizontalLine = $(page+'.join-line__vertical');

                    let fnLineBottom = function () {

                        let line = $(page2+'.join-line__vertical');
                        let pulse = $(page2+'.pulse');

                        TweenMax.to(line, lineSpeed, {height:'100%'});
                        TweenMax.to(pulse, lineSpeed, {opacity:1});

                    };

                    let fntriagle = function () {
                        TweenMax.to('.triagle-top-right', lineSpeed, {opacity:'1',onComplete: fnLineBottom});
                    };


                    let fnSquareLeft = function () {
                        let square = $(page+'.join-line__square--bottom-left');

                        TweenMax.to(square, squareSpeed, {opacity:'1',onComplete:fntriagle});
                    };

                    let fnLineHorizontal = function () {
                        let  verticalLine = $(page+'.join-line__gorizontal');
                        TweenMax.to(verticalLine, lineSpeed, {width:'100%',onComplete: fnSquareLeft});
                    };

                    let fnSquareRight = function () {
                        let square = $(page+'.join-line__square--bottom-right');

                        TweenMax.to(square, squareSpeed, {opacity:'1',onComplete: fnLineHorizontal});
                    };

                    TweenMax.to(horizontalLine, lineSpeed, {height:'100%',onComplete: fnSquareRight});
                };
                TweenLite.delayedCall(0,fnLine);



                $(window).resize(function () {
                    fnWidth();
                });


                let video = $('.security-section__block-before');


                video.addClass('animated fadeInLeft');

                //
                // setTimeout(function () {
                //     $("#security-section-video").YTPPlay();
                // },3200);

            }

            if (anchorLink === 'spec'){

                let page = '#join-line-spec ';
                let page2 = '#join-line-spec-2 ';
                let section = $('.spec-section');
                let joinLine_1 = $('#join-line-spec.join-line');
                let joinLine_2 = $('#join-line-spec-2.join-line');



                function fnWidth() {

                    let joinLineWidth = $(window).width() - section.offset().left - section.width() - 50;
                    let joinLineHeight = section.offset().top + 49;
                    let joinLineHeight2 = $('.spec-block').offset().top;
                    let joinLine2MargBottom = section.offset().top - 49;
                    joinLine_1.height(joinLineHeight-15 + 'px').width(joinLineWidth + 'px');
                    joinLine_2.height(joinLineHeight2 + 'px').css('margin-top',-joinLine2MargBottom + 'px').width(joinLineWidth + 'px');
                }

                fnWidth();


                let fnLine = function () {

                    const lineSpeed = 1;
                    const squareSpeed = .5;



                    let horizontalLine = $(page+'.join-line__vertical');


                    let fnLineHorizontal = function () {
                        let  verticalLine = $(page+'.join-line__gorizontal');
                        TweenMax.to(verticalLine, lineSpeed, {width:'100%'});
                    };

                    let fnSquareLeft = function () {
                        let square = $(page+'.join-line__square--bottom-left');

                        TweenMax.to(square, squareSpeed, {opacity:'1',onComplete: fnLineHorizontal});
                    };

                    TweenMax.to(horizontalLine, lineSpeed, {height:'100%',onComplete: fnSquareLeft});
                };
                let fnLine2 = function () {

                    const lineSpeed = 1;
                    const squareSpeed = .5;



                    let horizontalLine = $(page2+'.join-line__gorizontal');

                    let fnPulse = function () {
                        let  pulse = $(page2+'.pulse');
                        TweenMax.to(pulse, lineSpeed, {opacity:1});
                    }

                    let fnLineVertical = function () {
                        let  verticalLine = $(page2+'.join-line__vertical');
                        TweenMax.to(verticalLine, lineSpeed, {height:'100%',onComplete: fnPulse});
                    };

                    let fnSquareRight= function () {
                        let square = $(page2+'.join-line__square--top-right');

                        TweenMax.to(square, squareSpeed, {opacity:'1',onComplete: fnLineVertical});
                    };

                    TweenMax.to(horizontalLine, lineSpeed, {width:'100%',onComplete: fnSquareRight});
                };
                TweenLite.delayedCall(0,fnLine);
                TweenLite.delayedCall(4,fnLine2);



                $(window).resize(function () {
                    fnWidth();
                });




                let rightBlockAnimation = function () {
                    let speed = 1;
                    let completeRightAnimation = function () {



                        let wrapper = this.target;
                        let line = wrapper.getElementsByClassName('spec-block__inner-right-line')[0];
                        let background = wrapper.getElementsByClassName('spec-block__inner-bg')[0];
                        let title = wrapper.getElementsByClassName('spec-block__title')[0];
                        let description = wrapper.getElementsByClassName('spec-block__desc')[0];


                        let showTitleDescription = function () {
                            title.style.opacity = '1';
                            setTimeout(function () {
                                description.style.opacity = '1';
                            },100)
                        };

                        let showBg = function () {
                            TweenMax.to(background, speed / 2, {width:'100%', onComplete: showTitleDescription});
                        };

                        TweenMax.to(line, speed / 2, {height:'100%', onComplete:showBg});

                    };
                    let allComplete = function () {
                       // console.log('rightBlockAnimation complete')
                    };
                    TweenMax.staggerTo(".spec-block__item",1, {onComplete: completeRightAnimation }, speed);


                },

                    leftBlockAnimation = function () {

                    let leftBlocks = $('.spec-section__row .spec-section__block');


                    TweenMax.staggerTo(leftBlocks,1, {opacity: 1}, .5);


                },
                    imageAnimation = function () {
                    let image = $('.spec-section__image');
                    image.addClass('animated rotateInDownLeft');
                    let imgs = $('.spec-section__img-item');

                    let i = 0;

                    let imgSpeed = 8;

                    setInterval(function () {
                        i++;
                       if (i === imgs.length){
                           i = 0;
                       }
                        //imgs.removeClass('animated rotateInDownLeft flipInY');
                        imgs.addClass('zoomOut');
                        imgs.removeClass('zoomIn rotateInDownLeft');
                        imgs[i].classList.remove('zoomOut');
                        imgs[i].classList +=(' animated zoomIn');

                    },imgSpeed*1000);

                    setTimeout(function () {
                        leftBlockAnimation();
                        rightBlockAnimation();
                    },1000)
                };

                imageAnimation();

            }

            if (anchorLink === 'community'){

                let page = '#join-line-community ';
                let page2 = '#join-line-community-2 ';

                let section = $('.page__community .page__container');
                let joinLine = $('#join-line-community.join-line');

                let joinLine2 = $('#join-line-community-2.join-line');

                function fnWidth() {

                    let joinLineWidth = $(window).width() - section.offset().left - section.width() - 60;
                    let joinLineHeight = section.offset().top + 50;
                    joinLine.height(joinLineHeight + 'px').width(joinLineWidth + 'px');

                    let mTop = -50;
                    joinLine2.height(joinLineHeight + 'px').width(joinLineWidth + 'px').css({
                        'margin-top': mTop+'px'
                    });
                }

                fnWidth();

                let fnLine = function () {

                    const lineSpeed = 1;
                    const squareSpeed = .5;



                    let verticalLine = $(page+'.join-line__vertical');


                    let fnSquareLeft = function () {
                        let square = $(page+'.join-line__square--bottom-left');

                        TweenMax.to(square, squareSpeed, {opacity:'1'});
                    };

                    let fnLineHorizontal = function () {
                        let  horizontalLine = $(page+'.join-line__gorizontal');
                        TweenMax.to(horizontalLine, lineSpeed, {width:'100%',onComplete: fnSquareLeft});
                    };

                    let fnSquareRight = function () {
                        let square = $(page+'.join-line__square--bottom-right');

                        TweenMax.to(square, squareSpeed, {opacity:'1',onComplete: fnLineHorizontal});
                    };

                    TweenMax.to(verticalLine, lineSpeed, {height:'100%',onComplete: fnSquareRight});
                };


                let fnLine2 = function () {

                    const lineSpeed = 1;
                    const squareSpeed = .5;



                

                    let squareLeft = $(page2+'.join-line__square--top-left');

                    let fnPulse = function(){
                        let pulse = $(page2+'.pulse');
                        TweenMax.to( pulse, 0, {opacity:'1'});
                    }

                    let fnVerticalLine = function () {
                        let verticaLine = $(page2+'.join-line__vertical');
                        TweenMax.to(verticaLine, lineSpeed, {height:'100%',onComplete: fnPulse});
                    };

                    let fnSquareRight = function () {
                        let square = $(page2+'.join-line__square--top-right');
                        TweenMax.to(square, squareSpeed, {opacity:'1',onComplete: fnVerticalLine});
                    };

                    let fnLineHorizontal = function () {
                        let  horizontalLine = $(page2+'.join-line__gorizontal');
                        TweenMax.to(horizontalLine, lineSpeed, {width:'100%',onComplete: fnSquareRight});
                    };

                  

                    TweenMax.to(squareLeft, squareSpeed, {opacity:'1',onComplete: fnLineHorizontal});
                };

                TweenLite.delayedCall(1,fnLine);
                TweenLite.delayedCall(4,fnLine2);

                $(window).resize(function () {
                    fnWidth();
                });

                setTimeout(function () {
                    $('.page__community .page__container').addClass('animated lightSpeedIn');

                },200);

                setTimeout(function () {
                    $("#bgndVideo-community").YTPPlay();
                },3200);
               

            }

            if (anchorLink === 'token'){



                let page = '#join-line-token ';
                let page2 = '#join-line-token-2 ';
                let section = $('.page__token .page__container');
                let joinLine = $('#join-line-token.join-line');
                let joinLine2 = $('#join-line-token-2.join-line');

                function fnWidth() {

                    let joinLineWidth = $(window).width() - section.offset().left - section.width() - 60;
                    let joinLineHeight = $('.medal').offset().top + 100;
                    let joinLineHeight2 = $(window).height()-$('.token-price__title').offset().top ;
                    joinLine.height(joinLineHeight + 'px').width(joinLineWidth + 'px').css('margin-bottom', -joinLineHeight + 100 + 'px');
                    joinLine2.height(joinLineHeight2-40 + 'px').width(joinLineWidth + 'px').css('margin-top', -157+40 + 'px');
                }

                fnWidth();

                let fnLine = function () {

                    const lineSpeed = 1;
                    const squareSpeed = .5;



                    let verticalLine = $(page+'.join-line__vertical');


                    let fnLineHorizontal = function () {
                        let  horizontalLine = $(page+'.join-line__gorizontal');
                        TweenMax.to(horizontalLine, lineSpeed, {width:'100%'});
                    };

                    let fnSquareRight = function () {
                        let square = $(page+'.join-line__square--bottom-right');

                        TweenMax.to(square, squareSpeed, {opacity:'1',onComplete: fnLineHorizontal});
                    };

                    TweenMax.to(verticalLine, lineSpeed, {height:'100%',onComplete: fnSquareRight});
                };
                let fnLine2 = function () {

                    const lineSpeed = 1;
                    const squareSpeed = .5;

                    let square = $(page2+'.join-line__square--top-right');


                    let fnPulse = function () {
                        let pulse = $(page2+'.pulse');
                        TweenMax.to(pulse, squareSpeed, {opacity:'1'});

                    };

                    let fnVerticalLine = function () {
                        let verticalLine = $(page2+'.join-line__vertical');

                        TweenMax.to(verticalLine, squareSpeed, {height:'100%',onComplete: fnPulse});
                    };

                    TweenMax.to(square, lineSpeed, {opacity:'1',onComplete: fnVerticalLine});
                };

                TweenLite.delayedCall(1,fnLine);
                TweenLite.delayedCall(4,fnLine2);

                $(window).resize(function () {
                    fnWidth();
                });





                let speed = .2;

                let completeAnimation = function () {
                  let block = this.target;
                    TweenMax.to(block, 1, {opacity:1});
                };
                let allComplete = function () {
                    $('.token-price').addClass('animated bounceInUp')
                };
                TweenMax.staggerTo(".token-section__p",speed, {onComplete: completeAnimation }, speed, allComplete);

            }

            if(anchorLink === 'development'){


                let page = '#join-line-development ';
                let page2 = '#join-line-development-2 ';
                let section = $('.page__development .page__container');
                let joinLine = $('#join-line-development.join-line');
                let joinLine2 = $('#join-line-development-2.join-line');

                function fnWidth() {


                    let lineMarBottom = Number;
                    function DetectDevice(){
                        switch(DEVICE){
                            case 'small':
                                lineMarBottom = 100;
                                break;
                            case 'old':
                                lineMarBottom = 123;
                                break;
                            default:
                                lineMarBottom = 47;
    
                        }
    
                    }
                  
                    ResizeFn(DetectDevice);

                    let joinLineWidth = $('.development-section__item:first-child span').offset().left / 2 ;
                    let joinLineHeight = $('.development-section__cyrcle').offset().top + 10;
                    let joinLineHeight2 = $(window).height()-$('.development-section__cyrcle').offset().top ;


      

                  
                    joinLine.height(joinLineHeight + 'px').width(joinLineWidth + 'px').css({
                            'margin-bottom': -joinLineHeight/2 - lineMarBottom + 'px',
                            'margin-right': -joinLineWidth + 20 + 'px',
                        }
                    );
                    joinLine2.height(joinLineHeight2-40 + 'px').width(joinLineWidth + 'px').css('margin-top', -157+40 + 'px');
                }

                fnWidth();

                let fnLine = function () {

                    const lineSpeed = 1;
                    const squareSpeed = .5;


                    let verticalLine = $(page+'.join-line__vertical');

                    let fnHorizontalLine = function () {
                        let horizontalLine = $(page+'.join-line__gorizontal');

                        TweenMax.to(horizontalLine, squareSpeed, {width:'100%'});
                    };

                    let fnSquareRight = function () {
                        let square = $(page+'.join-line__square--bottom-left');

                        TweenMax.to(square, squareSpeed, {opacity:'1',onComplete: fnHorizontalLine});
                    };

                    TweenMax.to(verticalLine, lineSpeed, {height:'100%',onComplete: fnSquareRight});
                };
                let fnLine2 = function () {

                    const lineSpeed = 1;
                    const squareSpeed = .5;

                    let square = $(page2+'.join-line__square--top-left');


                    let fnPulse = function () {
                        let pulse = $(page2+'.pulse');
                        TweenMax.to(pulse, squareSpeed, {opacity:'1'});

                    };

                    let fnVerticalLine = function () {
                        let verticalLine = $(page2+'.join-line__vertical');

                        TweenMax.to(verticalLine, squareSpeed, {height:'100%',onComplete: fnPulse});
                    };

                    TweenMax.to(square, lineSpeed, {opacity:'1',onComplete: fnVerticalLine});
                };

                TweenLite.delayedCall(0,fnLine);
                TweenLite.delayedCall(10,fnLine2);

                $(window).resize(function () {
                    fnWidth();
                });




                let title = $('.development-section__title');

                title.addClass('animated fadeIn');


                let speed = .4;

                let completeAnimation = function () {
                    let wrapper = this.target,
                        icon = wrapper.getElementsByClassName('development-section__icon')[0],
                        sublist = wrapper.getElementsByClassName('development-section__sublist')[0],
                        stage = wrapper.getElementsByClassName('development-section__stage')[0],
                        line = wrapper.getElementsByClassName('development-section__cyrcle-after')[0];

                    let lineShow = function () {
                        if(line)
                            TweenMax.to(line,speed*2,{width:0});
                        else
                            return false;
                    };
                    let sublistShow = function () {
                        if(sublist)
                            TweenMax.to(sublist,speed,{opacity:1,onComplete:lineShow});
                        else
                            return false;
                    };
                    let stageShow = function () {
                        if(icon)
                            TweenMax.to(stage,speed,{opacity:1,onComplete:sublistShow});
                        else
                            return false;
                    };
                    TweenMax.to(icon,speed,{opacity:1,onComplete:stageShow});
                };

                let allComplete = function () {
                   // console.log('development coplete')
                };

                TweenMax.staggerTo(".development-section__item",1, {onComplete: completeAnimation }, speed*5, allComplete);
            }

            if(anchorLink === 'team'){





                let page = '#join-line-team ';
                let page2 = '#join-line-team-2 ';
                let section = $('.page__team .page__container');
                let joinLine = $('#join-line-team.join-line');
                let joinLine2 = $('#join-line-team-2.join-line');

                function fnWidth() {
                    let joinLineWidth = section.width() - $('.team-section__title span').offset().left - 80;
                    let joinLineHeight = section.offset().top + 15;
                     joinLine.height(joinLineHeight + 'px').width(joinLineWidth + 'px').css({
                        'margin-left': -joinLineWidth + 'px',
                    }
                     );

                     let mTop;


                     function DetectDevice(){
                        switch(DEVICE){
                            case 'small':
                               mTop = -50;
                               break;
                           case 'old':
                               mTop = -90;
                               break;
                           default:
                               mTop = -10;
                        }
                     }
                
                     ResizeFn(DetectDevice) ; 
                  
                        

                     joinLine2.height(joinLineHeight + 'px').width(joinLineWidth + 'px').css({
                        'margin-top': mTop + 'px',
                    }
                     );
                }

                fnWidth();

                let fnLine = function () {

                    const lineSpeed = 1;
                    const squareSpeed = .5;


                    let verticalLine = $(page+'.join-line__vertical');



                    let fnSquareLeft= function () {
                        let square = $(page+'.join-line__square--bottom-left');

                        TweenMax.to(square, squareSpeed, {opacity:'1'});
                    };

                    let fnHorizontalLine = function () {
                        let horizontalLine = $(page+'.join-line__gorizontal');

                        TweenMax.to(horizontalLine, squareSpeed, {width:'100%',onComplete: fnSquareLeft});
                    };


                    let fnSquareRight = function () {
                        let square = $(page+'.join-line__square--bottom-right');

                        TweenMax.to(square, squareSpeed, {opacity:'1',onComplete: fnHorizontalLine});
                    };

                    TweenMax.to(verticalLine, lineSpeed, {height:'100%',onComplete: fnSquareRight});
                };
                let fnLine2 = function () {

                    const lineSpeed = 1;
                    const squareSpeed = .5;


                

                    let square = $(page2+'.join-line__square--top-right');

                    let fnPulse = function(){
                        let pulse = $(page2 + '.pulse');
                        TweenMax.to(pulse,0,{opacity:1});
                    };

                    let fnVerticalLine = function () {
                        let verticalLine = $(page2+'.join-line__vertical');

                        TweenMax.to(verticalLine, lineSpeed, {height:'100%',onComplete:fnPulse});
                    };

                    TweenMax.to(square, squareSpeed, {opacity:'1',onComplete: fnVerticalLine});
                };

                TweenLite.delayedCall(0,fnLine);
                TweenLite.delayedCall(3,fnLine2);
                $(window).resize(function () {
                    fnWidth();
                });

                let title = $('.team-section__title');
                let blocks = $('.team-section__list');
                let item = blocks.children();

                title.addClass('animated fadeIn');
                item.eq(0).addClass('animated bounceInLeft');
                item.eq(1).addClass('animated bounceInDown');
                item.eq(2).addClass('animated bounceInRight');
                item.eq(3).addClass('animated bounceInLeft');
                item.eq(4).addClass('animated bounceInUp');
                item.eq(5).addClass('animated bounceInRight');


                let delay = .5;
                item.eq(0).css('animation-delay', delay+'s');
                item.eq(1).css('animation-delay', delay*2+'s');
                item.eq(2).css('animation-delay', delay*3+'s');
                item.eq(3).css('animation-delay', delay*4+'s');
                item.eq(4).css('animation-delay', delay*5+'s');
                item.eq(5).css('animation-delay', delay*6+'s');



            }

            if(anchorLink === 'faq') {






                let page = '#join-line-faq ';
                let page2 = '#join-line-faq-2 ';
                let section = $('.page__faq .page__container');
                let joinLine = $('#join-line-faq.join-line');
                let joinLine2 = $('#join-line-faq-2.join-line');

                function fnWidth() {
                    let lineMarTop = 30;
                    let lineMarTop2 = 30;
                    let joinLineWidth =  section.offset().left;
                    let joinLineHeight = section.offset().top + lineMarTop;
                     joinLine.height(joinLineHeight + 'px').width(joinLineWidth + 'px').css({
                         'margin-bottom': -lineMarTop + 'px'
                     });
                     joinLine2.height(joinLineHeight + 'px').width(joinLineWidth + 'px').css({
                        'margin-top': -lineMarTop2 + 'px'
                    });;
                }

                fnWidth();

                let fnLine = function () {

                    const lineSpeed = 1;
                    const squareSpeed = .5;


                    let verticalLine = $(page+'.join-line__vertical');



                    let fnSquareLeft= function () {
                        let square = $(page+'.join-line__square--bottom-left');

                        TweenMax.to(square, squareSpeed, {opacity:'1'});
                    };

                    let fnHorizontalLine = function () {
                        let horizontalLine = $(page+'.join-line__gorizontal');

                        TweenMax.to(horizontalLine, squareSpeed, {width:'100%',onComplete: fnSquareLeft});
                    };


                    let fnSquareRight = function () {
                        let square = $(page+'.join-line__square--bottom-right');

                        TweenMax.to(square, squareSpeed, {opacity:'1',onComplete: fnHorizontalLine});
                    };

                    TweenMax.to(verticalLine, lineSpeed, {height:'100%',onComplete: fnSquareRight});
                };

                let fnLine2 = function () {

                    const lineSpeed = 1;
                    const squareSpeed = .5;


                    let square = $(page2+'.join-line__square--top-left');

                    let fnPulse = function(){
                        let pulse = $(page2+'.pulse');
                        TweenMax.to(pulse, squareSpeed, {opacity:1});
                    }
                    let fnVerticalLine= function () {
                        let verticalLine = $(page2+'.join-line__vertical');

                        TweenMax.to(verticalLine, lineSpeed, {height:'100%',onComplete: fnPulse});
                    };

            
                    TweenMax.to(square, squareSpeed, {opacity:1,onComplete: fnVerticalLine});
                };


                TweenLite.delayedCall(0,fnLine);
                TweenLite.delayedCall(16,fnLine2);
                //TweenLite.delayedCall(3,fnLine2);


                ResizeFn(fnWidth);


                setTimeout(function () {
                    $("#bgndVideo-faq").YTPPlay();
                },3000)

                let wrapper1 = $('.faq-section__items-wrapper:first-child');
                let wrapper2 = $('.faq-section__items-wrapper:last-child');

                let speed = .52;




                if(!FAQ_seen){

                    $('.faq-section__item-title').each(function (i) {
                        let text = $(this).text();
                        $(this).css('min-height', $(this).height() + 10 + 'px')
                        $(this).before('<div class="typed-strings" id="typed-'+i+'"><p>'+text+'</p></div>');
                        $(this).html('<span id="text-typed-'+i+'"></span>');

                    });

                    setTimeout(function () {

                        for (let i = 0; i <  $('.faq-section__item-title').length;i++){
                            setTimeout(function () {
                                $('#typed-'+i).parent('.faq-section__item').css('opacity',1)
                                setTimeout(function () {
                                    new Typed('#text-typed-'+i, {
                                        stringsElement: '#typed-'+i,
                                        cursorChar: ''
                                    });
                                },500);
                            },1200*i)
                        }
                    },1000);
                }


                FAQ_seen = true;

                for(let i = 0; i<wrapper1.children().length;i++){
                    wrapper1.children().eq(i).addClass('animated ');//bounceInLeft
                    wrapper1.children().eq(i).css('animation-delay', i*speed+'s');
                }
                for(let i = 0; i<wrapper2.children().length;i++){
                    wrapper2.children().eq(i).addClass('animated ');//bounceInRight
                    wrapper2.children().eq(i).css('animation-delay', i*speed+'s');
                }

            }


        },
    });





    $("#community-section-video").YTPlayer();




    let desktop = function () {
        let block = $('.desktop-detect-js');


        if (block.is(':visible')){


            let medalRotation = function () {
                TweenLite.set(".medal", {perspective:800});
                TweenLite.set(".medal__wrapper", {transformStyle:"preserve-3d"});
                TweenLite.set(".medal__face--back", {rotationY:-180});
                TweenLite.set([".medal__face--back", ".medal__face"], {backfaceVisibility:"hidden"});

                let tl = new TimelineMax();
                tl.staggerTo($(".medal__wrapper"), 2, {rotationY:-180,ease:Linear.easeNone, repeat:-1, yoyo:false}, 2);


            };

            medalRotation();

            //pie chart
            var o = {
                init: function(){
                    this.diagram();
                },
                random: function(l, u){
                    return Math.floor((Math.random()*(u-l+1))+l);
                },
                diagram: function(){
                    var r = Raphael('diagram', 600, 600),
                        rad = 100,
                        defaultText = 'Skills',
                        speed = 250;

                    r.circle(300, 300, 85).attr({ stroke: 'none', fill: '#fff' });

                    var title = r.text(300, 300, defaultText).attr({
                        font: '20px Arial',
                        fill: '#fff'
                    }).toFront();

                    r.customAttributes.arc = function(value, color, rad){
                        var v = 3.6*value,
                            alpha = v == 360 ? 359.99 : v,
                            random = o.random(91, 240),
                            a = (random-alpha) * Math.PI/180,
                            b = random * Math.PI/180,
                            sx = 300 + rad * Math.cos(b),
                            sy = 300 - rad * Math.sin(b),
                            x = 300 + rad * Math.cos(a),
                            y = 300 - rad * Math.sin(a),
                            path = [['M', sx, sy], ['A', rad, rad, 0, +(alpha > 180), 1, x, y]];
                        return { path: path, stroke: color }
                    }

                    $('.get').find('.arc').each(function(i){
                        var t = $(this),
                            color = t.find('.color').val(),
                            value = t.find('.percent').val(),
                            text = t.find('.text').text();

                        rad += 30;
                        var z = r.path().attr({ arc: [value, color, rad], 'stroke-width': 26 });

                        z.mouseover(function(){
                            this.animate({ 'stroke-width': 50, opacity: .75 }, 1000, 'elastic');
                            if(Raphael.type != 'VML') //solves IE problem
                                this.toFront();
                            title.stop().animate({ opacity: 0 }, speed, '>', function(){
                                this.attr({ text: text + '\n' + value + '%' }).animate({ opacity: 1 }, speed, '<');
                            });
                        }).mouseout(function(){
                            this.stop().animate({ 'stroke-width': 26, opacity: 1 }, speed*4, 'elastic');
                            title.stop().animate({ opacity: 0 }, speed, '>', function(){
                                title.attr({ text: defaultText }).animate({ opacity: 1 }, speed, '<');
                            });
                        });
                    });

                }
            }
            $(function(){ o.init(); });

        }else {

            $.fn.fullpage.destroy('all');
        }

        if(block.is(':visible')){
           // console.log('desktop')
        }

    };


    desktop();

    $('.menu__btn').click(function () {
        $('body').addClass('activeMenu');
    });
    $('.close-btn,.menu__link').click(function () {
        $('body').removeClass('activeMenu');
    });



    let togglesFunc = function () {




        let wrapper = $('.faq-section__items-wrappe'),
            item    = $('.faq-section__item'),
            title   = $('.faq-section__item-title'),
            info    = $('.faq-section__item-info');

        title.click(function () {
            let wrapper = $(this).parent(item);


            if(!wrapper.hasClass('active')){
                item.removeClass('active').find(info).slideUp();
                wrapper.addClass('active').find(info).slideDown();
            }else {
                wrapper.removeClass('active').find(info).slideUp();
            }

        });

    };
    togglesFunc();





  



    // community_btn.click(function () {
    //
    //
    //     if( $(this).hasClass('videoPlay')){
    //         $(this).find('span').text('watch video');
    //         $(this).removeClass('videoPlay');
    //         $("#community-section-video").YTPPause();
    //     }else{
    //         $(this).addClass('videoPlay');
    //         $("#community-section-video").YTPPlay();
    //         $(this).find('span').text('stop video');
    //     }
    //
    //
    // });



    $("#bgndVideo-faq, #bgndVideo-community ").YTPlayer();


$('.security-section__block-before .video__screen').click(function () {



   $('.security-section__youtubeVideo').show()
   $('.security-section__youtubeVideo iframe')[0].src += "&autoplay=1";
    // $("#security-section-video").YTPlayer();
    // $('.security-section__titleImg').hide();

    //window.open('https://youtu.be/ys8Arbd8rTQ', '_blank');
});




if(DEVICE === 'mobile'){

    $(window).scroll(function() {
        if ($(document).scrollTop() > 300) {
            $('.page__header').removeClass('firstSlide');
        }
        else {
            $('.page__header').addClass('firstSlide');
        }
    });


    function ScrollTo(btn){
        $('.menu__item[data-menuanchor="'+btn+'"]').click(function(){
            $.scrollTo('.section[data-anchor="'+btn+'"]',{
                duration: 800 
            });
         })
    }
    ScrollTo('firstSection');
    ScrollTo('vision');
    ScrollTo('features');
    ScrollTo('save');
    ScrollTo('spec');
    ScrollTo('community');
    ScrollTo('token');
    ScrollTo('development');
    ScrollTo('team');
    ScrollTo('faq');

}



