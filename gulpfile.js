const   gulp 			= require( 'gulp' ),
        browserSync    	= require('browser-sync'),
        autoprefixer    = require('gulp-autoprefixer'),
        uglify         = require('gulp-uglify'),
        concat         = require('gulp-concat'),
        cleanCSS       = require('gulp-clean-css'),
        sass 			= require( 'gulp-sass' ),
    babel          = require('gulp-babel');
var gutil = require('gulp-util');
gulp.task('browser-sync', ()=> {
    browserSync({
        port: 5002,
        server: {
            baseDir: "./"
        },
        open: true,
        notify: false
        //
    });
});

gulp.task('min-css',function () {
    gulp.src('assets/css/base.css')
        .pipe(concat('base.min.css'))
        .pipe(cleanCSS('base.min.js'))
        .pipe(gulp.dest('assets/css/'))
});

gulp.task('min-js',function () {
    gulp.src('assets/js/es5/main.js')
        .pipe(concat('main.min.js'))
        .pipe(uglify('').on('error', function(err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
            this.emit('end');
        }))
        .pipe(gulp.dest('assets/js/es5/'))
});

gulp.task('babel', function() {
    return gulp.src('assets/js/main.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('assets/js/es5/'))
        .pipe(browserSync.reload({stream: true}))
});


gulp.task( 'sass', function() {
    gulp.src('./assets/sass/*.sass')
        .pipe( sass().on( 'error', sass.logError ) )
        .pipe(autoprefixer(['last 15 versions']))
        .pipe( gulp.dest( './assets/css' ) )
        .pipe(browserSync.reload({stream: true}));
});


gulp.task( 'watch', ['sass','browser-sync'], ()=> {
    gulp.watch( './assets/sass/**/*.sass', [ 'sass' ] );
    gulp.watch( './*.html', browserSync.reload);
    gulp.watch( './assets/js/*.js', browserSync.reload);

});


gulp.task('default', ['watch']);